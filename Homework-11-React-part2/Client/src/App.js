import "./App.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import React, { useEffect, useState } from "react";

function App() {
  const [weather, setWeather] = useState([]);
  useEffect(() => {
    fetch("https://localhost:5001/weatherForecast").then(async (response) => {
      setWeather(await response.json());
    });
  }, []);
  return (
    <div className="App">
      <div className="Head">Weather</div>
      <div className="Column">
        <TableContainer className="Table">
          <Table className="TableHead">
            <TableHead>
              <TableRow>
                <TableCell align="left">Date</TableCell>
                <TableCell align="left">Temperature&nbsp;(C)</TableCell>
                <TableCell align="left">Temperature&nbsp;(F)</TableCell>
                <TableCell align="left">Summary</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {weather.map((row) => (
                <TableRow
                  key={row.summary}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell align="left">{row.date}</TableCell>
                  <TableCell align="left">{row.temperatureC}</TableCell>
                  <TableCell align="left">{row.temperatureF}</TableCell>
                  <TableCell align="left">{row.summary}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default App;
